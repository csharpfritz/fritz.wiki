﻿using FritzWiki.Models;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FritzWiki
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public static ViewModels.Settings Settings { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton(Configuration);

            // Add framework services.
            services.AddMvc();

            // Configure database connection
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<WikiContext>(options => options.UseNpgsql(Configuration.GetConnectionString("db")),
                ServiceLifetime.Scoped);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceScopeFactory scopeFactory, WikiContext context)
        {

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            scopeFactory.SeedData();

            Settings = context.SiteSettings;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {

                routes.MapAreaRoute(
                    name: "area-admin",
                    areaName: "admin",
                    template: "admin/{controller}/{action}/{id?}",
                    defaults: new {area="Admin", controller="Home", action="Index"}
                );

                routes.MapRoute(
                    name: "error-handler",
                    template: "error",
                    defaults: new {controller="Home", action="Error"});

                routes.MapRoute(
                    name: "wikiArticle",
                    template: "{articleName}",
                    defaults: new { controller = "Home", action="Index", articleName="Home Page" }
                );

            });
        }
    }
}
