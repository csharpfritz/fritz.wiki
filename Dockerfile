FROM microsoft/aspnetcore
WORKDIR /app
COPY bin/Debug/netcoreapp1.1/publish/. .

EXPOSE 80

ENTRYPOINT ["dotnet", "FritzWiki.dll"]