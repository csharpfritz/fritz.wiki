using System;
using FritzWiki.Models;

namespace FritzWiki.ViewModels {

    public class User {
        public int ID { get; private set; }
        public string Logon { get; private set; }
        public string PasswordHash { get; private set; }
        public Guid Salt { get; private set; }
        public int InvalidLoginCount { get; private set; }
        public DateTime LockoutDateUtc { get; private set; }

        public User SetPassword(string oldPassword, string newPassword) {

            if (PasswordHash == "" || HashPassword(oldPassword) == PasswordHash) {
                PasswordHash = HashPassword(newPassword);
                LockoutDateUtc = Constants.MAXDATE;
                this.InvalidLoginCount = 0;
            }

            return this;

        }

        private string HashPassword(string clearText) {

            var saltedPassword = string.Concat(Logon, clearText, Salt.ToString());

            return string.Empty;

        }

        public static implicit operator User(Models.UserNode node) {

            return new User {

                ID = node.NodeID,
                Logon = node.Node.Name,
                PasswordHash = node.PasswordHash,
                Salt = node.Salt,
                InvalidLoginCount = node.InvalidLoginCount,
                LockoutDateUtc = node.LockoutDateUtc

            };

        }

    }

}