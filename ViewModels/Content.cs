using System;

namespace FritzWiki.ViewModels
{

    public class Content
    {

        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime LastUpdateUTC { get; set; }
        
        public string Markdown {get;set;}

        public static explicit operator Content(Models.ContentNode cNode) {

            return new Content {
                ID = cNode.NodeID,
                Name = cNode.Node.Name,
                LastUpdateUTC = cNode.Node.LastUpdateUtc,
                Markdown = cNode.Markdown
            };

        }

    }

}