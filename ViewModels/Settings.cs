using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using Newtonsoft.Json;

namespace FritzWiki.ViewModels {

    public class Settings {

        private Dictionary<string,string> _ValuesObject;

        public int NodeID { get; set; }

        public string Host { get; set; }

        public string Values { get; set; }

        public string this[string key]
        {
            get { return _ValuesObject[key]; }
            set {   }
        }

        private void BuildValuesObject() {

            if (_ValuesObject != null) return;

            _ValuesObject = new Dictionary<string,string>();

            if (string.IsNullOrEmpty(Values)) return;

            var reader = new JsonTextReader(new StringReader(Values));
            var token = "";
            var value = "";


            while (reader.Read()) {

                 if (reader.Value != null) {

                    if (reader.TokenType == JsonToken.PropertyName) {
                        token = reader.Value.ToString();
                    } else if (reader.TokenType == JsonToken.String) {
                        value = reader.Value.ToString();
                        _ValuesObject.Add(token, value);
                        token = "";
                        value = "";
                    }

                }

            }


        }

        public static explicit operator Settings(Models.SettingsNode node) {

            var newSettings = new Settings {
                NodeID = node.NodeID,
                Host = node.Host,
                Values = node.Values
            };

            newSettings.BuildValuesObject();

            return newSettings;

        }

    }

}