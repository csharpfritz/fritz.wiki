using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FritzWiki.ViewComponents {

    // [ViewComponent(Name="Footer")]
    public class Footer : ViewComponent {
        
        public async Task<IViewComponentResult> InvokeAsync(ViewModels.Content content, DateTime lastUpdate) {
            
            ViewBag.LastUpdate = lastUpdate;
            ViewBag.SiteName = Startup.Settings["SiteName"];
            return View(content);

        }

    }

}