using System.Linq;
using System.Threading.Tasks;
using FritzWiki.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FritzWiki.ViewComponents {

    public class Content :ViewComponent {

        public Content(Models.WikiContext context)
        {
            this.Repository = context;
        }

        public WikiContext Repository { get; }

        public async Task<IViewComponentResult> InvokeAsync(string contentName) {

            var content = await Repository.Content
                .Include(c => c.Node)
                .Where(c => c.Node.Name == contentName)
                .SingleOrDefaultAsync();

            return View(content);

        }

    }


}