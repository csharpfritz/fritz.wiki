﻿using System;
using System.Linq;
using FritzWiki.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FritzWiki.Controllers
{
    public class HomeController : Controller
    {
        public WikiContext Repository { get; private set; }

        public HomeController(FritzWiki.Models.WikiContext context)
        {
            this.Repository = context;
        }

        public IActionResult Index(string articleName = "Home Page")
        {
            
            var thisNode = Repository.Content
                .Include(c => c.Node)  
                .FirstOrDefault(n => n.Node.Name == articleName);

            if (thisNode == null) {

                ViewBag.ArticleName = articleName;   
                ViewBag.LastUpdate = DateTime.UtcNow;
                return View("NotFound");

            }

            return View((ViewModels.Content)thisNode);

        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
