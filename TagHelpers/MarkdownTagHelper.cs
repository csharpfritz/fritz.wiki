using System.Threading.Tasks;
using CommonMark;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System;
using System.Text;
using FritzWiki.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FritzWiki.TagHelpers
{
    [HtmlTargetElement("markdown", TagStructure = TagStructure.NormalOrSelfClosing)]
    [HtmlTargetElement(Attributes = "markdown")]
    public class MarkdownTagHelper : TagHelper
    {

        private readonly Dictionary<string,int> ReferencedArticles = new Dictionary<string, int>();

        public MarkdownTagHelper(ILoggerFactory loggerFactory)
        {
            this.Logger = loggerFactory.CreateLogger("MarkdownTagHelper");
        }

        public ModelExpression Content { get; set; }
        public ILogger Logger { get; }

        public async override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (output.TagName == "markdown")
            {
                output.TagName = "div";
            }
            output.Attributes.RemoveAll("markdown");

            var content = await GetContent(output);
            var markdown = content.Replace("&#xA;", "\n");

            IdentifyLinksToWikiArticles(markdown);

            Logger.LogInformation($"Before conversion: {content}");

            var html = CommonMarkConverter.Convert(markdown);

            Logger.LogInformation($"After conversion: {html}");

            html = html + FormatReferencedArticles();

            output.Content.SetHtmlContent(html ?? "");

         }

        private string FormatReferencedArticles()
        {
            
            if (ReferencedArticles.Count == 0) return string.Empty;

            var sb = new StringBuilder();

            sb.Append("<div class=\"row\"><h4>Referenced Content</h4></div>");
            sb.Append("<div class=\"row\">");

            var maxArticle = ReferencedArticles.Max(k => k.Value);

            foreach (var article in ReferencedArticles) {

                sb.Append($"<div class=\"col-sm-3 {ConvertViewsToClass(article.Value, maxArticle)}\"><a href=\"/{article.Key}\">{article.Key}</a></div>");

            }

            sb.Append("</div>");

            return sb.ToString();

        }

        private string ConvertViewsToClass(int articleCount, int maxArticle) {

            if (articleCount > maxArticle * 0.75m) {
                return "darkArticle";
            } else if (articleCount >= maxArticle * 0.5m) {
                return "mediumArticle";
            } else if (articleCount >= maxArticle * 0.25m) {
                return "lightArticle";
            }

            return "";

        }


        private void IdentifyLinksToWikiArticles(string markdown)
        {

            var reArticleName = new Regex(@"\(\/([^\)]+)\)", RegexOptions.Multiline);
            
            // Exit now if no match
            if (!reArticleName.IsMatch(markdown)) return;

            var matches = reArticleName.Matches(markdown);

            Logger.LogInformation($"{matches.Count} matches identified");

            // Replace the content of each match with a hyperlink
            foreach (Match match in matches)
            {
                var articleName = match.Groups[1].Captures[0];

                if (ReferencedArticles.ContainsKey(articleName.Value)) {
                    ReferencedArticles[articleName.Value] = ReferencedArticles[articleName.Value]+1;
                } else {
                    ReferencedArticles.Add(articleName.Value, 1);
                }
                // markdown = markdown.Replace(match.Captures[0].Value, $"<a href=\"/{articleName}\">{articleName}</a>");

                Logger.LogInformation($"Match identified: {articleName}");
            }

        }

        private async Task<string> GetContent(TagHelperOutput output)
        {
            if (Content == null)
                return (await output.GetChildContentAsync()).GetContent();

            return Content.Model?.ToString();
        }
    }
}
