using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FritzWiki.Models {

    public class SettingsNode {

        [Key]
        public int NodeID { get; set; }

        [ForeignKey("NodeID")]
        public Node Node { get; set; }

        public string Host { get; set; }

        public string Values { get; set; }

    }

}