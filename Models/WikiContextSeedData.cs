using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.DependencyInjection;

namespace FritzWiki.Models {

		public static class WikiContextSeedData {

		public static void SeedData(this IServiceScopeFactory factory) {
			
			using (var serviceScope = factory.CreateScope()) {

				var context = serviceScope.ServiceProvider.GetService<WikiContext>();
				if (!context.Nodes.Any()) {

					var rootNode = new Node { Name="Root Node", ParentNodeID=1};
					context.Nodes.Add(rootNode);
					context.SaveChanges();

					var nodeType = new Node { ParentNode=rootNode, Name="Node Type"};
					context.Add(nodeType);
					context.SaveChanges();

					var usersNode = new Node { ParentNode=nodeType, Name="Users"};
					var groupNode = new Node { ParentNode=nodeType, Name="Groups"};
					var partNode = new Node { ParentNode=nodeType, Name="ContentPart"};
					var contentNode = new Node { ParentNode=nodeType, Name="Content" };
					var settingsNode = new Node { ParentNode=nodeType, Name="Settings"};
					context.Nodes.AddRange(usersNode, groupNode, partNode, contentNode, settingsNode);
					context.SaveChanges();

					var defaultSettingsNode = new Node { ParentNode=settingsNode, Name="Default Site Settings" };
					var defaultSettings = new SettingsNode { Node= defaultSettingsNode, Host="*", Values="{ 'SiteName': 'Default FritzWiki'}"};
					context.Nodes.Add(defaultSettingsNode);
					context.Settings.Add(defaultSettings);
					context.SaveChanges();

					// Default Home Page
					var homePage = new Node { ParentNode=contentNode, Name="Home Page"};
					var homeContent = new ContentNode { Node = homePage, Markdown="**This** is the home page\n\nYou can find the [About](/About) page online as well" };
					context.Nodes.Add(homePage);
					context.Content.Add(homeContent);
					context.SaveChanges();

					// Default About Page
					var aboutPage = new Node { ParentNode=contentNode, Name="About"};
					var aboutContent = new ContentNode { Node=aboutPage, Markdown="This is the default site that is created when you install FritzWiki.\n\nYou can login with the default credentials\n\n<dl><dt>Login:</dt><dd>Fritz<dd><dt>Password:</dt><dd>TheCat</dd></dl>Once you are logged in, you can create a new user and assign it Administrative privileges as the owner of your wiki." };
					context.Nodes.Add(aboutPage);
					context.Content.Add(aboutContent);
					context.SaveChanges();

				}

			}

		}


	}

}