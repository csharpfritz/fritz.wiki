using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FritzWiki.Models {

    public class UserNode {

        [Key]
        public int NodeID { get; set; }

        [ForeignKey("NodeID")]
        public Node Node { get; set; }

        [Required]
        public Guid Salt { get; set; } = Guid.NewGuid();

        [Required]
        public string PasswordHash { get; set; }

        [Required]
        public int InvalidLoginCount { get; set; } = 0;

        public DateTime LockoutDateUtc {get;set;} = Constants.MAXDATE;

    }

}