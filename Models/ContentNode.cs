using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FritzWiki.Models {

    public class ContentNode {

        [Key]
        public int NodeID {get;set;}
        
        [ForeignKey("NodeID")]
        public Node Node {get;set;}

        public string Markdown { get; set; }

    }

}