using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FritzWiki.Models {

    public class Node {

        [Key()]
        public int ID { get; set; }

        public int? ParentNodeID { get; set; }

        [ForeignKey("ParentNodeID")]
        public Node ParentNode { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime CreateDateUtc { get; set; } = DateTime.UtcNow;

        [Required]
        public DateTime LastUpdateUtc { get; set; } = DateTime.UtcNow;

    }

}