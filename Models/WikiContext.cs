using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace FritzWiki.Models
{

    public class WikiContext : DbContext {
        
        public WikiContext(DbContextOptions<WikiContext> options) : base(options) {}

        public DbSet<Node> Nodes { get; set; }

        public DbSet<UserNode> Users { get; set; }

        public DbSet<ContentNode> Content { get; set; }

        public DbSet<SettingsNode> Settings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {

            builder.Entity<Node>()
                .HasAlternateKey(n => new {n.ParentNodeID, n.Name});

        }

        public ViewModels.Settings SiteSettings {
            get {
            
                var siteSettings = Settings
                    .Include(s => s.Node)
                    .First(s => s.Host == "*");

                return (ViewModels.Settings)siteSettings;

            }
        }
    } 

}