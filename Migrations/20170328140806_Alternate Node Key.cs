﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FritzWiki.Migrations
{
    public partial class AlternateNodeKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.DropIndex(
                name: "IX_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.AlterColumn<int>(
                name: "ParentNodeID",
                table: "Nodes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Nodes_ParentNodeID_Name",
                table: "Nodes",
                columns: new[] { "ParentNodeID", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID",
                principalTable: "Nodes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Nodes_ParentNodeID_Name",
                table: "Nodes");

            migrationBuilder.AlterColumn<int>(
                name: "ParentNodeID",
                table: "Nodes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID",
                principalTable: "Nodes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
