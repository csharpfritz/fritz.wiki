﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FritzWiki.Models;

namespace FritzWiki.Migrations
{
    [DbContext(typeof(WikiContext))]
    [Migration("20170325172556_Parent Node")]
    partial class ParentNode
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("FritzWiki.Models.Node", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("CreateDateUtc");

                    b.Property<DateTime>("LastUpdateUtc");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("ParentNodeID");

                    b.HasKey("ID");

                    b.HasIndex("ParentNodeID");

                    b.ToTable("Nodes");
                });

            modelBuilder.Entity("FritzWiki.Models.Node", b =>
                {
                    b.HasOne("FritzWiki.Models.Node", "ParentNode")
                        .WithMany()
                        .HasForeignKey("ParentNodeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
