﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FritzWiki.Migrations
{
    public partial class AddContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Content",
                table: "Nodes");

            migrationBuilder.CreateTable(
                name: "Content",
                columns: table => new
                {
                    NodeID = table.Column<int>(nullable: false),
                    Markdown = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Content", x => x.NodeID);
                    table.ForeignKey(
                        name: "FK_Content_Nodes_NodeID",
                        column: x => x.NodeID,
                        principalTable: "Nodes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Content");

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Nodes",
                nullable: true);
        }
    }
}
