﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FritzWiki.Migrations
{
    public partial class OptionalParentNodeID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.AlterColumn<int>(
                name: "ParentNodeID",
                table: "Nodes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID",
                principalTable: "Nodes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.AlterColumn<int>(
                name: "ParentNodeID",
                table: "Nodes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID",
                principalTable: "Nodes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
