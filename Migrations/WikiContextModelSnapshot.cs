﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FritzWiki.Models;

namespace FritzWiki.Migrations
{
    [DbContext(typeof(WikiContext))]
    partial class WikiContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("FritzWiki.Models.ContentNode", b =>
                {
                    b.Property<int>("NodeID");

                    b.Property<string>("Markdown");

                    b.HasKey("NodeID");

                    b.ToTable("Content");
                });

            modelBuilder.Entity("FritzWiki.Models.Node", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDateUtc");

                    b.Property<DateTime>("LastUpdateUtc");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("ParentNodeID")
                        .IsRequired();

                    b.HasKey("ID");

                    b.HasAlternateKey("ParentNodeID", "Name");

                    b.ToTable("Nodes");
                });

            modelBuilder.Entity("FritzWiki.Models.SettingsNode", b =>
                {
                    b.Property<int>("NodeID");

                    b.Property<string>("Host");

                    b.Property<string>("Values");

                    b.HasKey("NodeID");

                    b.ToTable("Settings");
                });

            modelBuilder.Entity("FritzWiki.Models.UserNode", b =>
                {
                    b.Property<int>("NodeID");

                    b.Property<int>("InvalidLoginCount");

                    b.Property<DateTime>("LockoutDateUtc");

                    b.Property<string>("PasswordHash")
                        .IsRequired();

                    b.Property<Guid>("Salt");

                    b.HasKey("NodeID");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("FritzWiki.Models.ContentNode", b =>
                {
                    b.HasOne("FritzWiki.Models.Node", "Node")
                        .WithMany()
                        .HasForeignKey("NodeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FritzWiki.Models.Node", b =>
                {
                    b.HasOne("FritzWiki.Models.Node", "ParentNode")
                        .WithMany()
                        .HasForeignKey("ParentNodeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FritzWiki.Models.SettingsNode", b =>
                {
                    b.HasOne("FritzWiki.Models.Node", "Node")
                        .WithMany()
                        .HasForeignKey("NodeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FritzWiki.Models.UserNode", b =>
                {
                    b.HasOne("FritzWiki.Models.Node", "Node")
                        .WithMany()
                        .HasForeignKey("NodeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
