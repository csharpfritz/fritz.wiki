﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FritzWiki.Migrations
{
    public partial class ParentNode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentNodeID",
                table: "Nodes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes",
                column: "ParentNodeID",
                principalTable: "Nodes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nodes_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.DropIndex(
                name: "IX_Nodes_ParentNodeID",
                table: "Nodes");

            migrationBuilder.DropColumn(
                name: "ParentNodeID",
                table: "Nodes");
        }
    }
}
